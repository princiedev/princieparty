﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterTower : MonoBehaviour
{
    public int waterLevel;
    public Text waterLevelText;
    private float incrementCounter;

    protected float Timer;
    protected float TimerChangeSpeed;
    // Start is called before the first frame update
    void Start()
    {
        waterLevel = 0;
        incrementCounter = 0.1f;

    }

    // Update is called once per frame
    void Update()
    {
        if (TimerChangeSpeed > 2)
        {
            int random = Random.Range(1, 4);
            if (random <= 2 && random > 1)
            {
                incrementCounter = 0.05f;
                TimerChangeSpeed = 0f;
            }
            else if (random <=3 && random > 2)
            {
                incrementCounter = 0.6f;
                TimerChangeSpeed = 0f;
            }
            else if (random <=4 && random > 3)
            {
                incrementCounter = 1f;
                TimerChangeSpeed = 0f;
            }
            
        }
        //incrementCounter = Random.Range(0.05f, 1f);
        Timer += Time.deltaTime;
        TimerChangeSpeed += Time.deltaTime;
        if (Timer >= incrementCounter)
        {
            Timer = 0f;
            waterLevel++;
            waterLevelText.text = waterLevel.ToString();
        }
        
    }

}
